import fs from 'fs'
import pkg from './package'

const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

const exampleFiles = fs
  .readdirSync('components/')
  .map(fileName => {
    const match = /(.*)-example.vue/.exec(fileName)
    return match && match[1]
  })
  .filter(x => x !== null)

const mapFiles = fs.readdirSync('static/maps/')

export default {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [],

  env: {
    exampleFiles,
    mapFiles
  },

  /*
   ** Customize the generated output folder
   */
  generate: {
    dir: 'public',
    routes: [...exampleFiles.map(fileName => `/${fileName}`)]
  },

  /*
   ** Customize the base url
   */
  router: {
    base
  }
}
