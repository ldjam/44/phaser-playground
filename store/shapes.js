export const state = () => ({
  colors: {
    circle: '#47b5d7',
    rectangle: '#88d735'
  },
  shape: null
})

export const mutations = {
  setColor(state, newValue) {
    state.colors[state.shape] = newValue
  },

  toggleShape(state) {
    state.shape = state.shape === 'circle' ? 'rectangle' : 'circle'
  }
}
