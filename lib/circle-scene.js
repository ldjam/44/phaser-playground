import Phaser from 'phaser'

const radius = 100

class CircleScene extends Phaser.Scene {
  create() {
    this.circle = new Phaser.Geom.Circle(radius, radius, radius)
    this.graphics = this.add.graphics()
  }

  setColor(newColorHex) {
    const { color } = Phaser.Display.Color.HexStringToColor(newColorHex)
    this.graphics.fillStyle(color)
  }

  update() {
    this.graphics.fillCircleShape(this.circle)
  }
}

export default CircleScene
