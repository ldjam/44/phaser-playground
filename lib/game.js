import Phaser from 'phaser'

class Game extends Phaser.Game {
  constructor(options) {
    super({
      width: 800,
      height: 600,
      type: Phaser.AUTO,
      scene: {},
      transparent: true,
      ...options
    })
  }
}

export default Game
