import Phaser from 'phaser'

class HpScene extends Phaser.Scene {
  constructor({ x, y, width, height, lineWidth, maxValue }) {
    super()
    Object.assign(this, { width, height, lineWidth, maxValue })

    this.strokeRectangle = new Phaser.Geom.Rectangle(
      x + lineWidth / 2,
      y + lineWidth / 2,
      width + lineWidth,
      height + lineWidth
    )
    this.fillRectangle = new Phaser.Geom.Rectangle(
      x + lineWidth,
      y + lineWidth,
      0,
      height
    )
    this.color = 0xffffff
  }

  create() {
    this.graphics = this.add.graphics()
  }

  setCurrentValue(newValue) {
    const value = Math.min(this.maxValue, Math.max(0, newValue))
    const ratio = value / this.maxValue

    this.fillRectangle.width = Math.round(this.width * ratio)
    this.color = new Phaser.Display.Color(
      Math.min(255, Math.round(2 * 255 * (1 - ratio))),
      Math.min(255, Math.round(2 * 255 * ratio)),
      0
    ).color
    this.redraw = true
  }

  update() {
    if (this.redraw) {
      this.graphics.clear()
      this.graphics.fillStyle(this.color)
      this.graphics.lineStyle(this.lineWidth, 0x000000)
      this.graphics.strokeRectShape(this.strokeRectangle)
      this.graphics.fillRectShape(this.fillRectangle)
      this.redraw = false
    }
  }
}

export default HpScene
