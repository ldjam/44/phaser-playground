import Phaser from 'phaser'

class MapScene extends Phaser.Scene {
  constructor(baseUrl, mapFilePath) {
    super({ key: mapFilePath })

    this.baseUrl = baseUrl
    this.mapFilePath = mapFilePath
  }

  tilesetCacheKey(name) {
    return `${this.mapFilePath}/tilesets/${name}`
  }

  preload() {
    this.load.setBaseURL(this.baseUrl)

    this.load.on('filecomplete', (key, type, data) => {
      if (key !== this.mapFilePath) {
        return
      }

      const { data: mapData } = data
      const { tilesets } = mapData
      tilesets.forEach(({ image, name }) => {
        const tilesetUrl = image.replace(/^\.\.\//, '') // this relies on maps/ and tilesets/ directory being at the same level
        this.load.image(this.tilesetCacheKey(name), tilesetUrl)
      })
    })

    this.load.tilemapTiledJSON(this.mapFilePath, this.mapFilePath)
  }

  create() {
    const map = this.make.tilemap({ key: this.mapFilePath })

    const { data: mapData } = this.cache.tilemap.get(this.mapFilePath)

    const { tilesets } = mapData
    const tilesetNames = tilesets.map(({ name }) => name)
    tilesetNames.forEach(name =>
      map.addTilesetImage(name, this.tilesetCacheKey(name))
    )

    const { layers } = mapData
    const layerNames = layers.map(layer => layer.name)
    layerNames.forEach(name => {
      map.createDynamicLayer(name, tilesetNames, 0, 0)
    })
  }

  update() {}
}

export default MapScene
