import Phaser from 'phaser'

const size = 200

class RectangleScene extends Phaser.Scene {
  create() {
    this.rectangle = new Phaser.Geom.Rectangle(0, 0, size, size)
    this.graphics = this.add.graphics()
  }

  setColor(newColorHex) {
    const { color } = Phaser.Display.Color.HexStringToColor(newColorHex)
    this.graphics.fillStyle(color)
  }

  update() {
    this.graphics.fillRectShape(this.rectangle)
  }
}

export default RectangleScene
