const path = require('path')

// make WebStorm / RubyMine happy
module.exports = {
  resolve: {
    alias: {
      '~': path.resolve(__dirname)
    }
  }
}
